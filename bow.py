from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report 
import pandas as pd

negative_sentences = [
    (line, 0) for line in open('data/rt-polaritydata/rt-polarity.neg', 'r', encoding='cp1252')
]

positive_sentences = [
    (line, 1) for line in open('data/rt-polaritydata/rt-polarity.pos', 'r', encoding='cp1252')
]

df = pd.DataFrame(data=negative_sentences + positive_sentences, columns=['sentence', 'target'])

X, y = df['sentence'], df['target']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state=42)

text_clf = Pipeline([
#    ('vect', CountVectorizer()),
    ('tfidf', TfidfVectorizer()),
    ('clf', MultinomialNB())
])

text_clf.fit(X_train, y_train)

y_pred = text_clf.predict(X_test)
print(classification_report(y_test, y_pred, target_names=['negative', 'positive']))