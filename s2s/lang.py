import torch


class Lang:
    PAD = 0
    UNK = 1
    SOS = 2
    EOS = 3
    MAX_LEN = 10

    def __init__(self):
        self.word2idx = {}
        self.word2cnt = {}
        self.idx2word = ['<pad>', '<unk>', '<sos>', '<eos>']
        self.num_words = 4

    def add_sentence(self, sentence):
        for word in sentence.split(' '):
            self.add_word(word)

    def add_word(self, word):
        if word not in self.word2idx:
            self.idx2word.append(word)
            self.num_words += 1
            self.word2idx[word] = self.num_words - 1
            self.word2cnt[word] = 1
        else:
            self.word2cnt[word] += 1

    def build_vocab(self, sentences):
        for sentence in sentences:
            self.add_sentence(sentence)

    def sentence2idx(self, sentence):
        return [self.word2idx.get(word, 2) for word in sentence.split(' ')]

    def idx2sentence(self, indices):
        def get_word(idx):
            return self.idx2word[idx] if idx > 2 else ''
        return ' '.join([get_word(idx) for idx in indices])

    def sentence2tensor(self, sentence, device):
        indices = self.sentence2idx(sentence) + [Lang.EOS]
        return torch.tensor(indices, dtype=torch.int, device=device)[:, None]
