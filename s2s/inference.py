import pickle
from s2s.data import preprocess_sentence

if __name__ == '__main__':
    with open(r'../model.pkl', 'rb') as f:
        model_dict = pickle.load(f)

    model = model_dict['model']
    source_lang = model_dict['source_lang']
    target_lang = model_dict['target_lang']

    while True:
        print('>>> Enter English sentence: ')
        s = input()
        if s:
            input_sentence = source_lang.sentence2tensor(preprocess_sentence(s), 'cpu')
            print(input_sentence)
            _, output_sentence  = model(input_sentence)
            print(output_sentence)
        print(f'<<< In French: {target_lang.idx2sentence(output_sentence)}')
