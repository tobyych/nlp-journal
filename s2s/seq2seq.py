from torch import nn
import torch

from s2s.lang import Lang
from s2s.encoder import Encoder
from s2s.decoder import Decoder

MAX_LENGTH = 20

class Seq2Seq(nn.Module):
  def __init__(self, input_size, output_size, embed_size=256, hidden_size=512, num_layers=1):
    super().__init__()

    # initialize the encoder and decoder
    self.encoder = Encoder(input_size, hidden_size, embed_size, num_layers)
    self.decoder = Decoder(output_size, hidden_size, embed_size, num_layers)

    self.device = 'cuda' if torch.cuda.is_available() else 'cpu'

  def forward(self, source):
    vocab_size = self.decoder.output_dim

    # encode every word in a sentence
    _, hidden = self.encoder(source)

    # add a token before the first predicted word
    decoder_input = torch.tensor([Lang.SOS], device=self.device)  # SOS

    output = torch.zeros((MAX_LENGTH, vocab_size), device=self.device)
    best_words = []
    for i in range(MAX_LENGTH):
        decoder_output, hidden = self.decoder(decoder_input, hidden)
        output[i] = decoder_output

        topv, topi = decoder_output.topk(1)
        decoder_input = topi.squeeze().detach()
        best_words.append(decoder_input)
        if decoder_input.item() == Lang.EOS:
            break

    return output, best_words
