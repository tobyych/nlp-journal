from s2s.seq2seq import MAX_LENGTH, Seq2Seq
from s2s.data import read_data, preprocess
from s2s.lang import Lang

from torch import optim, nn
import torch.cuda

import pickle
import numpy as np


NUM_EPOCHS = 100
LR = 1e-3
BATCH_SIZE = 32


def train(sentence_pairs, num_epochs=10000, lr=1e-3, batch_size=32):
    sentence_pairs = np.array(sentence_pairs)

    # train, validation split
    np.random.shuffle(sentence_pairs)
    train_end_idx = int(len(sentence_pairs) * 0.999) + 1
    train_data = sentence_pairs[:train_end_idx, :]
    valid_data = sentence_pairs[train_end_idx:, :]

    # build up language vocab
    source_lang = Lang()
    source_lang.build_vocab(train_data[:, 0])

    target_lang = Lang()
    target_lang.build_vocab(train_data[:, 1])

    model = Seq2Seq(
        input_size=source_lang.num_words,
        output_size=target_lang.num_words,
        num_layers=1
    )

    optimizer = optim.SGD(model.parameters(), lr=lr)
    criterion = nn.NLLLoss()

    # device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    # with torch.cuda.device(device if device.type == 'cuda' else None):

    #     model.cuda()

    device = torch.device('cpu')
    for i in range(num_epochs):
        model.train()

        indices = np.random.randint(0, len(train_data), batch_size)
        train_loss = 0
        for idx in indices:
            from_sentence = train_data[idx, 0]
            to_sentence = train_data[idx, 1]

            input = source_lang.sentence2tensor(from_sentence, device)
            target = target_lang.sentence2tensor(to_sentence, device)

            padded_output, _ = model(input)
            padding = MAX_LENGTH - len(target)
            padded_target = nn.functional.pad(target[:, -1], (0,padding))
            loss = criterion(padded_output, padded_target.long())
          
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            train_loss += loss.item() / batch_size

        if i % 50 == 0:
            model.eval()
            valid_loss = 0
            for row in valid_data:
                from_sentence = row[0]
                to_sentence = row[1]

                input = source_lang.sentence2tensor(from_sentence, device)
                target = target_lang.sentence2tensor(to_sentence, device)

                padded_output, _ = model(input)
                padding = MAX_LENGTH - len(target)
                padded_target = nn.functional.pad(target[:, -1], (0,padding))
                loss = criterion(padded_output, padded_target.long())

                valid_loss += loss.item() / len(valid_data)

            print(f'Iteration #{i}: {train_loss=:.02f}, {valid_loss=:.02f}')
        else:
            print(f'Iteration #{i}: {train_loss=:.02f}')

    return model, source_lang, target_lang


if __name__ == '__main__':
    filepath = 'eng-fra.txt'
    raw_sentence_pairs = read_data(filepath)

    sentences_pairs = preprocess(raw_sentence_pairs)

    model, source_lang, target_lang = train(sentences_pairs, num_epochs=NUM_EPOCHS,
                                            lr=LR, batch_size=BATCH_SIZE)

    bundle = {
        'model': model,
        'source_lang': source_lang,
        'target_lang': target_lang
    }

    with open(r'../model.pkl', 'wb') as f:
        pickle.dump(bundle, f)
