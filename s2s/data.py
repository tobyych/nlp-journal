import string


def read_data(filepath):
    sentences_pairs = []
    with open(filepath, 'r', encoding='utf8') as f:
        for line in f:
            i = 0
            sentence1 = ''
            while i < len(line):
                if line[i] == '\t':
                    break
                sentence1 += line[i]
                i += 1

            i += 1
            sentence2 = ''
            while i < len(line):
                if line[i] == '\t':
                    break
                sentence2 += line[i]
                i += 1

            sentences_pairs.append((sentence1, sentence2))
    return sentences_pairs


def preprocess(sentence_pairs):
    return [
        (preprocess_sentence(sentence1), preprocess_sentence(sentence2))
        for (sentence1, sentence2) in sentence_pairs
    ]


def preprocess_sentence(sentence):
    return sentence.lower() \
        .replace('[^A-Za-z\s]+', '') \
        .strip(string.punctuation) \
        .strip()
