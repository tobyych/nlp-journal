from __future__ import annotations
import heapq
import string
from dataclasses import dataclass, field
from typing import Optional
from functools import total_ordering
from ete3 import TreeNode

text = 'i love data science, i love big data. i am who i am.'


# text = '''
# One day the Hare laughedat the short feet and slow speed of the Tortoise. The Tortoise replied:
# "You may be as fast as the wind, but I will beat you in a race!"
# The Hare thought this idea was impossible and he agreed to the proposal. It was agreed that the Fox should choose the course and decide the end.
# The day for the race came, and the Tortoise and Hare started together.
# The Tortoise never stopped for a moment, walking slowly but steadily, right to the end of the course. The Hare ran fast and stopped to lie down for a rest. But he fell fast asleep. Eventually, he woke up and ran as fast as he could. But when he reached the end, he saw the Tortoise there already, sleeping comfortably after her effort.
# '''

text = text.translate(str.maketrans('', '', string.punctuation))
text = text.lower()
text = text.replace('\n', ' ')
text = text.strip()
text = text.split(' ')

word_counts = {}
text_size = len(text)
for word in text:
    try:
        word_counts[word] += 1 / text_size
    except:
        word_counts[word] = 1 / text_size

@total_ordering
@dataclass(eq=False)
class BaseNode:
  weight: float

  def __eq__(self, other):
    return self.weight == other.weight

  def __lt__(self, other):
    return self.weight <= other.weight

@dataclass
class InternalNode(BaseNode):
  left: Optional[InternalNode] = field(default=None, compare=False)
  right: Optional[InternalNode] = field(default=None, compare=False)
  parent: Optional[InternalNode] = field(default=None, compare=False)

@dataclass
class LeafNode(BaseNode):
  symbol: str = field(compare=False)
  parent: Optional[InternalNode] = field(default=None, compare=False)

priority_queue = []
for word, count in word_counts.items():
  leaf_node = LeafNode(count, word)
  heapq.heappush(priority_queue, leaf_node)

while len(priority_queue) > 1:
  first_child = heapq.heappop(priority_queue)
  second_child = heapq.heappop(priority_queue)
  node = InternalNode(first_child.weight + second_child.weight)
  node.left = first_child
  node.right = second_child
  heapq.heappush(priority_queue, node)

huffman_tree = priority_queue[0]

def add_node(node):
  
  if type(node) is LeafNode:
    ete_treenode = TreeNode()
    ete_treenode.add_feature('name', node.symbol)
  
  elif type(node) is InternalNode:

    if node.left is None and node.right is None:
      return None

    left = add_node(node.left)
    right = add_node(node.right)

    ete_treenode = TreeNode()
    if left:
      ete_treenode.add_child(left)

    if right:
      ete_treenode.add_child(right)

    ete_treenode.add_feature('weight', f'{node.weight:0.2f}')
  
  return ete_treenode

tree = add_node(huffman_tree)

print(tree.get_ascii(attributes=['name', 'weight']))